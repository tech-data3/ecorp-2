from pydantic.dataclasses import dataclass


@dataclass
class Product:
    name: str
    description: str
    reference: str
    price: float

@dataclass
class ProductL2(Product):
    old_price: float
    discount: float
