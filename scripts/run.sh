#!/bin/bash
cd ..

sudo mysql < init.sql

source venv/bin/activate
python level1.py
python level2.py
python level3.py
python level4.py
python level5.py