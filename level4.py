from requests_html import HTMLSession
import pandas as pd

from util import to_sql_wrapper

if __name__ == '__main__':
    url = 'http://localhost:8080/level4/'
    session = HTMLSession()
    r = session.get(url)
    r.html.render()


    table_list = pd.read_html(r.html.raw_html, header=0, flavor='bs4')
    df = table_list[0]
    df.columns=df.columns.str.lower()
    df.rename(columns={'#' : 'old_index', 'e-mail' : 'mail'}, inplace=True)


    to_sql_wrapper(df, 'user_l4')
