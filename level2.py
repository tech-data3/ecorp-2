import pandas as pd
import requests
from bs4 import BeautifulSoup

from Product import ProductL2
from util import to_sql_wrapper

if __name__ == '__main__':
    url = 'http://localhost:8080/level2/'
    html = requests.get(url)

    if not html.ok:
        raise Exception()

    soup = BeautifulSoup(html.text, 'html.parser')

    products = []
    products_info = soup.findAll('div', {'class': 'product-info'})
    for info in products_info:
        price_items = info.find('div', {'class': 'line'}).findChildren('strong', {'class': 'price'})
        prices = []
        for price in price_items:
            prices.append(float(price.text[:-1]))
        product = ProductL2(
            info.findChild('h2').string,
            info.findChild('p').string,
            info.findChild('small').string,
            min(prices),
            max(prices),
            -(min(prices) / max(prices)) + 1
        )
        products.append(product)
    df = pd.DataFrame(products)
    to_sql_wrapper(df, 'product_l2')
