import requests
from bs4 import BeautifulSoup
import pandas as pd

from Product import Product
from util import to_sql_wrapper, max_str_length

if __name__ == '__main__':
    url = 'http://localhost:8080/level5/0'
    html = requests.get(url)

    if not html.ok:
        raise Exception()

    soup = BeautifulSoup(html.text, 'html.parser')
    products = []
    
    while True:
        product_infos = soup.select('.product-info')
        for info in product_infos:
            product = Product(
                info.findChild('h2').string,
                info.findChild('p').string,
                info.findChild('small').string,
                float(info.find('div', {'class': 'line'}).findChild('strong', {'class': 'price'}).string[:-1]),
            )
            products.append(product)
        next_item = next(iter([e for e in soup.select('main > .line a') if e.text=='Next']),None)

        if next_item != None:

            html = requests.get(url + '/' + next_item.get('href'))
            soup = BeautifulSoup(html.text, 'html.parser')
        else :
            break
    df = pd.DataFrame(products)

    to_sql_wrapper(df, 'product_l5')

