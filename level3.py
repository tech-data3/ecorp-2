from dataclasses import dataclass
import requests
import pandas as pd

from util import to_sql_wrapper, create_engine_wrapper

if __name__ == '__main__':
    url = 'http://localhost:8080/level3/'
    html = requests.get(url)

    if not html.ok:
        raise Exception()
    table_list = pd.read_html(html.text, header=0, flavor='bs4')
    df = table_list[0]
    df.columns=df.columns.str.lower()
    df[['reference', 'name']] = df['name'].str.split(' ', expand=True, n=1)
    df['price'] = df['price'].apply(lambda x : float(x[:-1]))

    df_l2 = pd.read_sql('SELECT reference, name, price FROM product_l2', create_engine_wrapper())

    df_l2['name'] = df_l2['name'].apply(lambda x : x.lower().replace('/','').replace(' ', '').strip())
    df3 = df.merge(df_l2, on=['reference'])
    df3['price_diff_l2'] = df3.apply(lambda x: x['price_x']-x['price_y'],axis=1)
    df3.drop(columns=['price_y', 'name_y'], inplace=True)
    df3.rename(columns={'name_x' : 'name', 'price_x' : 'price'}, inplace=True)
    print(df3)
    to_sql_wrapper(df3, 'product_l3')
