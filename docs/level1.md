# Level 1

## Tasks

-[x] recognize what we have to do
-[x] search for the element that contain the info we want to retreive
-[x] prepare and imagine the databases

## What we want to do 

Récuperer tous les nom, prix, ref et description des produits sur cette page. 
 
## html dom structure and what we have to get

```html
...
<div class="product">
    <div class="product-img">
        <img src="https://picsum.photos/200" alt="Prody prod">
    </div>
    <div class="product-info">
        <h2>Cyclobenzaprine Hydrochloride</h2> #nom du produit
        <p>Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.</p> #description
        <small>6524ffcbfc13ae47f17a8255</small> #ref
        <div class="line">
            <strong class="price">851.69€</strong> #prix
            <a href="/404" class="btn">BUY !</a>
        </div>
    </div>
</div>
...
```
## database modelisation and creation

### MLD
![schema de la bdd](level1.drawio.png)


