# Level 1

## Tasks

-[ ] recognize what we have to do
-[ ] search for the element that contain the info we want to retreive
-[ ] prepare and imagine the databases

## What we  have to do 

Récuperer tous les nom, prix, ref et description des produits sur cette page.

Calculer pour chaque produit le taux de réduction entre le prix barré et le prix de vente.
 
## html dom structure and what we have to get

```html
...
<div class="product">
    <div class="product-img">
        <img src="https://picsum.photos/200" alt="Prody prod">
    </div>
    <div class="product-info">
        <h2>Viper RT/10</h2>
        <p>Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in,
            tempus sit amet, sem.</p>
        <small>335018670-X</small>
        <div class="line">


            <strong class="price"><s>908.71€</s></strong>
            <strong class="price">799.66€</strong>
            <a href="/404" class="btn">BUY !</a>
        </div>
    </div>
</div>
...
```

structure is the same as level 1
price is just different but easy to handle with min max

## database modelisation and creation

### MLD
![schema de la bdd](level1.drawio.png)


