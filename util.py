from sqlalchemy import create_engine


def create_engine_wrapper():
    return create_engine("mysql+pymysql://{user}:{pw}@{host}/{db}".format(
        user='<username>',
        pw='<password>',
        host='<host>',
        db='<db>'
    )
    )

def to_sql_wrapper(df, table):
    df.index += 1
    engine = create_engine_wrapper()
    return df.to_sql(
        table,
        engine,
        if_exists='append',
        index_label='id'
    )

def max_str_length(column):
    return len(max(column, key=lambda x : len(x)))
