DROP DATABASE IF EXISTS ecorp2;

CREATE DATABASE ecorp2;

use ecorp2;

CREATE TABLE product_l1(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(60) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    reference VARCHAR(30),
    description TEXT
);

CREATE TABLE product_l2(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(60) NOT NULL,
    old_price DECIMAL(10,2) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    discount DECIMAL(10,2) NOT NULL,
    reference VARCHAR(30),
    description TEXT
);

CREATE TABLE product_l3(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(60) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    reference VARCHAR(30),
    description TEXT NOT NULL,
    price_diff_l2 DECIMAL(10,2) NOT NULL
);

CREATE TABLE user_l4(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    old_index INT NOT NULL,
    name VARCHAR(20) NOT NULL,
    mail VARCHAR(30) NOT NULL
);

CREATE TABLE product_l5(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(60) NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    reference VARCHAR(30),
    description TEXT
);

GRANT ALL PRIVILEGES ON ecorp2.* TO 'npo'@'localhost';