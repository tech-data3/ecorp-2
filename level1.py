import requests
from bs4 import BeautifulSoup
import pandas as pd

from Product import Product
from util import to_sql_wrapper


if __name__ == '__main__':
    url = 'http://localhost:8080/level1/'
    html = requests.get(url)

    if not html.ok:
        raise Exception()

    soup = BeautifulSoup(html.text, 'html.parser')

    products = []
    products_info = soup.findAll('div', {'class': 'product-info'})
    for info in products_info:
        product = Product(
            info.findChild('h2').string,
            info.findChild('p').string,
            info.findChild('small').string,
            float(info.find('div', {'class': 'line'}).findChild('strong', {'class': 'price'}).string[:-1]),
        )
        products.append(product)
    df = pd.DataFrame(products)
    to_sql_wrapper(df, 'product_l1')


